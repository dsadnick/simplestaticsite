// JavaScript Document
function content1() {
	document.getElementById("rssFeed").innerHTML = "For the latest news on the Affordable Care Act, <a href='http://www.npr.org/blogs/health/' target='new'><span style='text-decoration:underline;'>Click Here</a>";
	var outputText1 =
"<h1>Group & Individual Medical Plans</h1>" +
"<p><b>Traditional Plans</b><br />" + 
"High-deductible plans that reduce premiums in exchange for 80/20<br />" + 
"coinsurance or 100% for covered expenses after the deductible.</p>" + 
"<p><b>Health Savings Account Plans</b><br />" + 
"An ideal choice that combines a lower-cost, high-deductible<br />" + 
"health insurance plan with a tax-favored savings account.</p>" + 
"<p><b>Short Term Health Insurance</b><br />" + 
"A great option for seasonal or part-time workers, or if<br />" + 
"you've lost group coverage through a recent job or life change." +
"<p><b>Dental Insurance</b><br />" + 
"Smile. We'll help you, and your family, find the dental insurance <br />" + 
"options that fit your budget.</p>" +
"<p><b>Life & Disability</b><br />" +
"This valuable insurance coverage gives you the peace of mind of<br />" +
"knowing that your loved ones’ finances are more secure.</p>" +
"<p><b>Vision Plans</b><br />" +
"We can offer a wealth of information about our vision plans and<br />" +
"providers. We can provide a total solution in high-quality, <br /> " +
"affordable vision care coverage.</p>" + 
"<p><b>Long-term care (LTC) insurance </b><br />" + 
"important coverage that provides valuable support and financial<br />" + 
"resources that help cover the cost of long-term care.</p>" + 
"<p><b>Worksite Voluntary Benefits </b><br />" + 
"Allow employees to select the benefits that are best suited for <br />" + 
"their own particular needs and fund these benefits themselves.</p>" + 
"<p><b>Partially Self-Funded Benefit Plans</b><br />" + 
"Self funding is an alternative financial strategy to costly<br />" + 
"traditional benefits ... The Benefit Works can help to set up<br />" +
"a benefits plandesign that best meets the unique needs of  <br />" + 
"employer and employee</p>";

document.getElementById("mainContent").innerHTML = outputText1; 

}



function content2() {
	document.getElementById("rssFeed").innerHTML = "For the latest news on the Affordable Care Act, <a href='http://www.npr.org/blogs/health/' target='new'><span style='text-decoration:underline;'>Click Here</a>";
	var outputText2 = 
"<h1>Service</h1>" +
"<p><b>Affordable Car Act Reform Guidance</b><br />" + 
"We can help you track Affordable Care Act (ACA) guidance, <br />" + 
"deadlines, changes and updates as they happen. Understand <br />" + 
"the impact of key requirements on your business.</p>" + 

"<p><b>Employer Compliance</b><br />" + 
"<h3>Health Care Reform Is Here – More Regulations Are Coming</h3>" +
"&bull;Shared Responsibility (2014) – Fewer than half of <br />" +
"midsized and large companies have estimated their exposure<br />" +
"to potential penalties<br />" + 
"&bull;Excise Tax Assessment (on so-called “Cadillac” Plans<br />" +
"in 2018) – Most companies have yet to assess whether they <br /> " +
"will be affected by the 40% excise tax (levied upon <br /> " +
"insurers but certain to be passed on to employers).</p>" +

"<p><b>Employee Education</b>" + 
"Many federal and state employees are still uninformed<br >" + 
"about how healthcare reform impacts their own insurance<br />" + 
"benefits. Although public sector workers are often at <br />" + 
"the front lines of legislative changes, they still require<br />" +
"their employers to provide them with information about how<br />" +
"these types of alterations affect them The BenefitWorks  <br />" + 
"can help.</p>" + 

"<p><b>Legislative updates<b><br />" + 
"These health reform updates can help you understand how the<br />"
"Affordable Care Act (ACA) impacts your plan. We encourage<br />" +
"you to work with your attorney and other business partners<br />" + 
"to comply with these changes.</p>"

"<p><b>HR Portal and Information</b><br />" + 
"Our Client Portal gives our clients the latest news on<br />" +
"any ACA changes</p>" + 

"<p><b>ACA Guidance</b><br />" +
"The BenefitWorks can advise our clients on all the changes<br />" + 
"and how to optimize (or minimize) the new law regarding <br />" +
"their business.</p>";	


document.getElementById("mainContent").innerHTML = outputText2;
}



function content3() {
	document.getElementById("rssFeed").innerHTML = "For the latest news on the Affordable Care Act, <a href='http://www.npr.org/blogs/health/' target='new'><span style='text-decoration:underline;'>Click Here</a>";
	var outputText3 = 
"<h1>Specialty Programs</h1>" + 
"<p><b>Employee Benefits Programs</b><br />" + 
"The purpose of employee benefits is to increase the <br />" +
"economic security of staff members, and in doing so,<br />" +
"improve worker retention across the organization. As <br />" +
"such, it is one component of reward management. We<br />" +
" can design a plan that fits your needs.</p>" + 
"<p><b>Rx Programs</b><br />" +
"We can help you design Rx programs, with discount<br />" +
"cards, to aid with employee retention.</p>" + 
"<p><b>Concierge Medicine</b><br />" +
"Concierge medicine (also known as direct care) is a<br />" +
"relationship between a patient and a primary care<br />" +
"physician in which the patient pays an annual fee or <br />" +
"retainer. This may or may not be in addition to other<br />" +
"charges. In exchange for the retainer, doctors provide<br />" + 
"enhanced care.</p>" + 
"<p><b>Medical Tourism</b><br />" +
"Medical tourism (MT) is patient movement from highly<br />" +
"developed nations to other areas of the world for<br />" +
"medical care, usually to find treatment at a lower <br />" + 
"cost, at world-class facilities, enjoying a mini-vacation<br />" +
" at the same time. Many times insurance companies approve,<br />" +
" because it lowers their cost.</p>" + 
"<p><b>Medical Advocacy</b><br />" +
"An area of lay specialization in health care concerned<br />" + 
"with patient education about the use of health plans<br />" +
"and how to obtain needed care.<p>" + 
"<p><b>Exchange Calculation Tools and Services</b><br />" +
"Not sure what goes here" + 
"<p><b>Property and Casualty Associations</b><br />" +
"Not sure what goes here</p>";
	
document.getElementById("mainContent").innerHTML = outputText3;
}
